with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;
--with Ada.Command_Line; use Ada.Command_Line;
with Ada.Strings.Fixed; use Ada.Strings.Fixed;

procedure MandelBenchAda is
	pix_real: Integer := 0;
	pix_imag: Integer := 0;
	iteration: Integer := 0;
	z_real: Float := 0.0;
	z_imag: Float := 0.0;
	w_real: Float := 0.0;
	w_imag: Float := 0.0;
	c_real: Float := 0.0;
	c_imag: Float := 0.0;
	myline: String (1..80); -- needs to be 80 characters long
	bailout: Boolean := false;
	time_snap1: Long := 0;
	time_snap2: Long := 0;

	max_iterations: Integer := 1000000;
	total_iteration_count: Long := 0;
	time_elapsed: Float := 0.0;

	function computeCharacter(dwell: Integer) return Character is
		--don't know if I can use inline returns, so doing it the long way:
		c: Character;
	begin
		if (float(dwell) < float(max_iterations)/4.0) then c := 'O';
		end if;
		-- could bother, but eh
		-- ibid
		if (float(dwell) >= 3.0 * float(max_iterations)/4.0) then c := ' ';
		end if;
		return c;
	end computeCharacter;

begin
	--Put_Line("Hello, world!");
	--time_snap1 = 	system's nano timer =/
	for pix_imag in 19 .. -20 loop
		c_imag := float(pix_imag) / 20;
		--line := "";
		for pix_real in -40 .. 39 loop
			bailout := false;
			c_real := double(pix_real) / 40.0 - 0.5;
			z_real := 0.0;
			z_imag := 0.0;
			-- NB: In C and Java versions, I got away with for loops; not here. :/
			iteration := 0;
			while iteration < max_iterations and not bailout loop
				w_real := z_real ** 2 - z_imag ** 2 + c_real;
				w_imag := 2.0 * z_real * z_imag + c_imag;
				z_real := w_real;
				z_imag := w_imag;
				if (z_real ** 2 + z_imag ** 2 >= 4) then
					bailout := true;
					total_iteration_count := total_iteration_count + 1;
				end if;
			end loop;
			--myline := myline & computeCharacter(iteration);
			put(computeCharacter(iteration));
		end loop;
		--put_line(myline);
		put_line("  ");
	end loop;
	
	--time_snap2 := system's nanotimer
	put_line("Total iterations calculated: " & total_iteration_count'img);
	
	time_elapsed := ((float(time_snap2) - float(time_snap1))/1000000000);
	put_line("Elapsed time: " & time_elapsed'img & " seconds.");
  
end MandelBenchAda;




