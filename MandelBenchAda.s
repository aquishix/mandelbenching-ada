	.file	"mandelbenchada.adb"
	.text
	.type	mandelbenchada___clean.2166, @function
mandelbenchada___clean.2166:
.LFB2:
	.cfi_startproc
	.cfi_personality 0x3,__gnat_eh_personality
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%r10, %rax
	movq	(%rax), %rdx
	movq	%rdx, -16(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -8(%rbp)
	movq	-16(%rbp), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rax, %rsi
	call	system__secondary_stack__ss_release
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	mandelbenchada___clean.2166, .-mandelbenchada___clean.2166
	.section	.rodata
.LC5:
	.ascii	"  "
.LC6:
	.ascii	"Total iterations calculated: "
.LC8:
	.ascii	" seconds."
.LC9:
	.ascii	"Elapsed time: "
.globl _Unwind_Resume
	.text
.globl _ada_mandelbenchada
	.type	_ada_mandelbenchada, @function
_ada_mandelbenchada:
.LFB1:
	.cfi_startproc
	.cfi_personality 0x3,__gnat_eh_personality
	.cfi_lsda 0x3,.LLSDA1
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
.LEHB0:
	subq	$400, %rsp
	.cfi_offset 3, -32
	.cfi_offset 12, -24
	call	system__secondary_stack__ss_mark
.LEHE0:
	movq	%rax, %rcx
	movq	%rdx, %rax
	movq	%rcx, -400(%rbp)
	movq	%rax, -392(%rbp)
	movq	-400(%rbp), %rax
	movq	%rax, -320(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -312(%rbp)
	movq	-320(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -344(%rbp)
	movl	$0, -76(%rbp)
	movl	$0, -72(%rbp)
	movl	$0, -68(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -64(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -60(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -56(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -52(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -48(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -44(%rbp)
	movb	$0, -37(%rbp)
	movl	$1000000, -36(%rbp)
	movl	$0, -32(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -28(%rbp)
	movl	$-19, -24(%rbp)
.L10:
	cmpl	$20, -24(%rbp)
	jg	.L4
.LEHB1:
	cvtsi2ss	-24(%rbp), %xmm0
	movss	.LC1(%rip), %xmm1
	divss	%xmm1, %xmm0
	movss	%xmm0, -44(%rbp)
	movl	$-40, -20(%rbp)
.L9:
	cmpl	$39, -20(%rbp)
	jg	.L5
	movb	$0, -37(%rbp)
	cvtsi2ss	-20(%rbp), %xmm0
	movss	.LC2(%rip), %xmm1
	divss	%xmm1, %xmm0
	movss	.LC3(%rip), %xmm1
	subss	%xmm1, %xmm0
	movss	%xmm0, -48(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -64(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -60(%rbp)
	movl	$0, -68(%rbp)
	jmp	.L8
.L16:
	nop
.L8:
	movl	-68(%rbp), %eax
	cmpl	$999999, %eax
	setle	%dl
	movzbl	-37(%rbp), %eax
	xorl	$1, %eax
	andl	%edx, %eax
	testb	%al, %al
	je	.L6
	movss	-64(%rbp), %xmm0
	mulss	-64(%rbp), %xmm0
	movss	-60(%rbp), %xmm1
	mulss	-60(%rbp), %xmm1
	subss	%xmm1, %xmm0
	addss	-48(%rbp), %xmm0
	movss	%xmm0, -56(%rbp)
	movss	-64(%rbp), %xmm0
	addss	%xmm0, %xmm0
	mulss	-60(%rbp), %xmm0
	addss	-44(%rbp), %xmm0
	movss	%xmm0, -52(%rbp)
	movl	-56(%rbp), %eax
	movl	%eax, -64(%rbp)
	movl	-52(%rbp), %eax
	movl	%eax, -60(%rbp)
	movss	-64(%rbp), %xmm0
	movaps	%xmm0, %xmm1
	mulss	-64(%rbp), %xmm1
	movss	-60(%rbp), %xmm0
	mulss	-60(%rbp), %xmm0
	addss	%xmm1, %xmm0
.LEHE1:
.LEHB2:
	ucomiss	.LC4(%rip), %xmm0
.LEHE2:
	setae	%al
	testb	%al, %al
	je	.L16
	movb	$1, -37(%rbp)
	movl	-32(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -32(%rbp)
	jmp	.L8
.L6:
	movl	-68(%rbp), %eax
	movl	%eax, %edi
.LEHB3:
	call	mandelbenchada__computecharacter.2191
	movzbl	%al, %eax
	movl	%eax, %edi
	call	ada__text_io__put__2
	addl	$1, -20(%rbp)
	jmp	.L9
.L5:
	movl	$.LC5, %eax
	movq	%rax, -304(%rbp)
	movq	$C.6.2293, -296(%rbp)
	movq	-304(%rbp), %rdx
	movq	-296(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rax, %rsi
	call	ada__text_io__put_line__2
	addl	$1, -24(%rbp)
	jmp	.L10
.L4:
	leaq	-368(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	$C.7.2296, -280(%rbp)
	movq	-288(%rbp), %rcx
	movq	-280(%rbp), %rdx
	movl	-32(%rbp), %eax
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	system__img_int__image_integer
	movl	%eax, %edx
	movslq	%edx, %rdx
	movl	$0, %ecx
	testq	%rdx, %rdx
	cmovs	%rcx, %rdx
	movq	%rdx, -264(%rbp)
	movl	%eax, %edx
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	movl	%eax, %edx
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	leaq	-368(%rbp), %rdx
	movl	$1, -240(%rbp)
	movl	%eax, -236(%rbp)
	movq	%rdx, -256(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -248(%rbp)
	movl	$.LC6, %eax
	movq	%rax, -224(%rbp)
	movq	$C.14.2315, -216(%rbp)
	movq	-256(%rbp), %rdx
	movq	-248(%rbp), %rcx
	movq	-224(%rbp), %rbx
	movq	-216(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	system__string_ops__str_concat
	movq	%rax, %rcx
	movq	%rdx, %rax
	movq	%rcx, -400(%rbp)
	movq	%rax, -392(%rbp)
	movq	-400(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rax, %rsi
	call	ada__text_io__put_line__2
	leaq	-384(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	$C.15.2319, -184(%rbp)
	movq	-192(%rbp), %rbx
	movq	-184(%rbp), %rcx
	movl	$0, %eax
	movl	$0, %edx
	movq	%rax, (%rsp)
	movl	%edx, 8(%rsp)
	movl	$6, %edx
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	call	system__img_real__image_floating_point
	movl	%eax, %edx
	movslq	%edx, %rdx
	movl	$0, %ecx
	testq	%rdx, %rdx
	cmovs	%rcx, %rdx
	movq	%rdx, -168(%rbp)
	movl	%eax, %edx
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	movl	%eax, %edx
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	movl	$.LC8, %edx
	movq	%rdx, -160(%rbp)
	movq	$C.20.2334, -152(%rbp)
	leaq	-384(%rbp), %rdx
	movl	$1, -128(%rbp)
	movl	%eax, -124(%rbp)
	movq	%rdx, -144(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -136(%rbp)
	movl	$.LC9, %eax
	movq	%rax, -112(%rbp)
	movq	$C.23.2341, -104(%rbp)
	movq	-160(%rbp), %rdi
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rcx
	movq	-112(%rbp), %rbx
	movq	-104(%rbp), %rax
	movq	%rdi, %r8
	movq	%rsi, %r9
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	system__string_ops_concat_3__str_concat_3
	movq	%rax, %rcx
	movq	%rdx, %rax
	movq	%rcx, -400(%rbp)
	movq	%rax, -392(%rbp)
	movq	-400(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rax, %rsi
	call	ada__text_io__put_line__2
.LEHE3:
	jmp	.L15
.L14:
.L12:
	movl	%edx, %ebx
	movq	%rax, %r12
	leaq	-352(%rbp), %rax
	movq	%rax, %r10
.LEHB4:
	call	mandelbenchada___clean.2166
	movq	%r12, %rax
	movslq	%ebx, %rdx
	movq	%rax, %rdi
	call	_Unwind_Resume
.L15:
	leaq	-352(%rbp), %rax
	movq	%rax, %r10
	call	mandelbenchada___clean.2166
	addq	$400, %rsp
	popq	%rbx
	popq	%r12
	leave
	.cfi_def_cfa 7, 8
.LEHE4:
	ret
	.cfi_endproc
.LFE1:
	.size	_ada_mandelbenchada, .-_ada_mandelbenchada
.globl __gnat_eh_personality
	.section	.gcc_except_table,"a",@progbits
.LLSDA1:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1-.LLSDACSB1
.LLSDACSB1:
	.uleb128 .LEHB0-.LFB1
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB1
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L14-.LFB1
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB1
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB3-.LFB1
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L14-.LFB1
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB1
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1:
	.text
	.type	mandelbenchada__computecharacter.2191, @function
mandelbenchada__computecharacter.2191:
.LFB3:
	.cfi_startproc
	.cfi_personality 0x3,__gnat_eh_personality
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -20(%rbp)
	cvtsi2ss	-20(%rbp), %xmm0
	movss	.LC10(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	seta	%al
	testb	%al, %al
	je	.L18
	movb	$79, -1(%rbp)
.L18:
	cvtsi2ss	-20(%rbp), %xmm0
	ucomiss	.LC11(%rip), %xmm0
	setae	%al
	testb	%al, %al
	je	.L19
	movb	$32, -1(%rbp)
.L19:
	movzbl	-1(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	mandelbenchada__computecharacter.2191, .-mandelbenchada__computecharacter.2191
	.section	.rodata
	.align 4
	.type	C.6.2293, @object
	.size	C.6.2293, 8
C.6.2293:
	.long	1
	.long	2
	.align 4
	.type	C.7.2296, @object
	.size	C.7.2296, 8
C.7.2296:
	.long	1
	.long	11
	.align 4
	.type	C.14.2315, @object
	.size	C.14.2315, 8
C.14.2315:
	.long	1
	.long	29
	.align 4
	.type	C.15.2319, @object
	.size	C.15.2319, 8
C.15.2319:
	.long	1
	.long	12
	.align 4
	.type	C.20.2334, @object
	.size	C.20.2334, 8
C.20.2334:
	.long	1
	.long	9
	.align 4
	.type	C.23.2341, @object
	.size	C.23.2341, 8
C.23.2341:
	.long	1
	.long	14
	.align 4
.LC1:
	.long	3248488448
	.align 4
.LC2:
	.long	1109393408
	.align 4
.LC3:
	.long	1056964608
	.align 4
.LC4:
	.long	1082130432
	.align 4
.LC10:
	.long	1215570944
	.align 4
.LC11:
	.long	1228348160
	.ident	"GCC: (GNU) 4.4.6 20110731 (Red Hat 4.4.6-3)"
	.section	.note.GNU-stack,"",@progbits
