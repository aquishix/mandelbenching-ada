with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;
--with Ada.Command_Line; use Ada.Command_Line;
with Ada.Strings.Fixed; use Ada.Strings.Fixed;
with Ada.Real_Time; use Ada.Real_Time;


procedure MandelBenchAda is
	pix_real: Integer := 0;
	pix_imag: Integer := 0;
	iteration: Integer := 0;
	z_real: Float := 0.0;
	z_imag: Float := 0.0;
	w_real: Float := 0.0;
	w_imag: Float := 0.0;
	c_real: Float := 0.0;
	c_imag: Float := 0.0;
	--myline: String (1..80); -- needs to be 80 characters long
	bailout: Boolean := false;
	time_snap1: Ada.Real_Time.Time;
	time_snap2: Ada.Real_Time.Time;

	max_iterations: Integer := 1000000;
	total_iteration_count: Integer := 0; -- should be Long
	time_elapsed: Float := 0.0;

	function computeCharacter(dwell: Integer) return Character is
		--don't know if I can use inline returns, so doing it the long way:
		c: Character;
	begin
		if (float(dwell) < float(max_iterations)/4.0) then c := 'O';
		end if;
		-- could bother, but eh
		-- ibid
		if (float(dwell) >= 3.0 * float(max_iterations)/4.0) then c := ' ';
		end if;
		return c;
	end computeCharacter;

begin
	time_snap1 := Ada.Real_Time.Clock;
	for pix_imag in -19 .. 20 loop -- I wish I could do this the natural way
		c_imag := -1.0 * float(pix_imag) / 20.0;
		--line := "";
		for pix_real in -40 .. 39 loop
			bailout := false;
			c_real := float(pix_real) / 40.0 - 0.5;
			z_real := 0.0;
			z_imag := 0.0;
			-- NB: In C and Java versions, I got away with for loops; not here. :/
			iteration := 0;
			while (iteration < max_iterations and not bailout) loop
				total_iteration_count := total_iteration_count + 1;
				w_real := z_real ** 2 - z_imag ** 2 + c_real;
				w_imag := 2.0 * z_real * z_imag + c_imag;
				z_real := w_real;
				z_imag := w_imag;
				if (z_real ** 2 + z_imag ** 2 >= 4.0) then bailout := true;
					
				end if;
				iteration := iteration + 1;
			end loop;
			--myline := myline & computeCharacter(iteration);
			put(computeCharacter(iteration));
		end loop;
		--put_line(myline);
		put_line("  ");
	end loop;
	
	time_snap2 := Ada.Real_Time.Clock;
	put_line("Total iterations calculated: " & total_iteration_count'img);
	
	--time_elapsed := ((float(time_snap2) - float(time_snap1))/1000.0);
	time_elapsed := float( To_Duration( time_snap2 - time_snap1 ) );
	put_line("Elapsed time: " & time_elapsed'img & " seconds.");
  
end MandelBenchAda;




