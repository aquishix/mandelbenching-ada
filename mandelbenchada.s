	.file	"mandelbenchada.adb"
	.text
	.align 2
	.type	mandelbenchada__computecharacter.2266, @function
mandelbenchada__computecharacter.2266:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	movq	%rsp, %rbp
	.cfi_offset 6, -16
	.cfi_def_cfa_register 6
	movl	%edi, -20(%rbp)
	movq	%r10, -32(%rbp)
	cvtsi2ss	-20(%rbp), %xmm0
	movaps	%xmm0, %xmm1
	movss	.LC6(%rip), %xmm0
	ucomiss	%xmm1, %xmm0
	seta	%al
	testb	%al, %al
	je	.L2
	movb	$79, -1(%rbp)
.L2:
	cvtsi2ss	-20(%rbp), %xmm0
	ucomiss	.LC7(%rip), %xmm0
	setae	%al
	testb	%al, %al
	je	.L3
	movb	$32, -1(%rbp)
.L3:
	movzbl	-1(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	mandelbenchada__computecharacter.2266, .-mandelbenchada__computecharacter.2266
	.section	.rodata
.LC14:
	.ascii	"  "
.LC15:
	.ascii	"Total iterations calculated: "
.LC17:
	.ascii	" seconds."
.LC18:
	.ascii	"Elapsed time: "
	.align 4
.LC0:
	.long	1
	.long	2
	.align 4
.LC1:
	.long	1
	.long	11
	.align 4
.LC2:
	.long	1
	.long	29
	.align 4
.LC3:
	.long	1
	.long	12
	.align 4
.LC4:
	.long	1
	.long	9
	.align 4
.LC5:
	.long	1
	.long	14
	.text
	.align 2
.globl _ada_mandelbenchada
	.type	_ada_mandelbenchada, @function
_ada_mandelbenchada:
.LFB1:
	.cfi_startproc
	.cfi_personality 0x3,__gnat_personality_v0
	.cfi_lsda 0x3,.LLSDA1
	pushq	%rbp
	.cfi_def_cfa_offset 16
	movq	%rsp, %rbp
	.cfi_offset 6, -16
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
.LEHB0:
	subq	$496, %rsp
	movl	$0, -64(%rbp)
	movl	$0, -68(%rbp)
	movl	$0, -36(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -40(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -44(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -72(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -76(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -80(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -84(%rbp)
	movb	$0, -45(%rbp)
	movl	$1000000, -88(%rbp)
	movl	$0, -52(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -92(%rbp)
	.cfi_offset 3, -48
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	ada__real_time__clock
	movq	%rax, -104(%rbp)
	movl	$-19, -56(%rbp)
.L12:
	cmpl	$20, -56(%rbp)
	jg	.L6
	cvtsi2ss	-56(%rbp), %xmm0
	movss	.LC9(%rip), %xmm1
	divss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	movss	.LC10(%rip), %xmm0
	xorps	%xmm1, %xmm0
	movss	%xmm0, -84(%rbp)
	movl	$-40, -60(%rbp)
.L11:
	cmpl	$39, -60(%rbp)
	jg	.L7
	movb	$0, -45(%rbp)
	cvtsi2ss	-60(%rbp), %xmm0
	movss	.LC11(%rip), %xmm1
	divss	%xmm1, %xmm0
	movss	.LC12(%rip), %xmm1
	subss	%xmm1, %xmm0
	movss	%xmm0, -80(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -40(%rbp)
	movl	$0x00000000, %eax
	movl	%eax, -44(%rbp)
	movl	$0, -36(%rbp)
.L10:
	cmpl	$999999, -36(%rbp)
	setle	%dl
	movzbl	-45(%rbp), %eax
	xorl	$1, %eax
	andl	%edx, %eax
	testb	%al, %al
	je	.L8
	addl	$1, -52(%rbp)
	movss	-40(%rbp), %xmm0
	mulss	-40(%rbp), %xmm0
	movss	-44(%rbp), %xmm1
	mulss	-44(%rbp), %xmm1
	subss	%xmm1, %xmm0
	addss	-80(%rbp), %xmm0
	movss	%xmm0, -72(%rbp)
	movss	-40(%rbp), %xmm0
	addss	%xmm0, %xmm0
	mulss	-44(%rbp), %xmm0
	addss	-84(%rbp), %xmm0
	movss	%xmm0, -76(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, -40(%rbp)
	movl	-76(%rbp), %eax
	movl	%eax, -44(%rbp)
	movss	-40(%rbp), %xmm0
	mulss	-40(%rbp), %xmm0
	movaps	%xmm0, %xmm1
	movss	-44(%rbp), %xmm0
	mulss	-44(%rbp), %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LC13(%rip), %xmm0
	setae	%al
	testb	%al, %al
	je	.L9
	movb	$1, -45(%rbp)
.L9:
	addl	$1, -36(%rbp)
	jmp	.L10
.L8:
	movl	-36(%rbp), %eax
	leaq	-448(%rbp), %rdx
	movq	%rdx, %r10
	movl	%eax, %edi
	call	mandelbenchada__computecharacter.2266
	movzbl	%al, %eax
	movl	%eax, %edi
	call	ada__text_io__put__2
	addl	$1, -60(%rbp)
	jmp	.L11
.L7:
	movl	$.LC14, %eax
	movq	%rax, -448(%rbp)
	movq	$.LC0, -440(%rbp)
	movq	-448(%rbp), %rdx
	movq	-440(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rax, %rsi
	call	ada__text_io__put_line__2
	addl	$1, -56(%rbp)
	jmp	.L12
.L6:
	call	ada__real_time__clock
.LEHE0:
	movq	%rax, -112(%rbp)
	movq	%rsp, %rax
	movq	%rax, %r14
	leaq	-464(%rbp), %rax
	movq	%rax, -432(%rbp)
	movq	$.LC1, -424(%rbp)
	movq	-432(%rbp), %rcx
	movq	-424(%rbp), %rdx
	movl	-52(%rbp), %eax
	movq	%rcx, %rsi
	movl	%eax, %edi
.LEHB1:
	call	system__img_int__image_integer
	movl	%eax, %edx
	movl	%edx, %eax
	movl	$0, %ecx
	testl	%eax, %eax
	cmovs	%ecx, %eax
	leal	29(%rax), %ebx
	movl	%ebx, -116(%rbp)
	movl	$0, %eax
	testl	%ebx, %ebx
	cmovns	%ebx, %eax
	cltq
	movq	%rax, -128(%rbp)
	movl	$0, %eax
	testl	%ebx, %ebx
	cmovns	%ebx, %eax
	cltq
	addq	$15, %rax
	addq	$15, %rax
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	16(%rsp), %rax
	addq	$15, %rax
	shrq	$4, %rax
	salq	$4, %rax
	movq	%rax, -136(%rbp)
	leaq	-464(%rbp), %rax
	movq	%rax, -416(%rbp)
	movl	$1, -400(%rbp)
	movl	%edx, %eax
	movl	%eax, -396(%rbp)
	leaq	-400(%rbp), %rax
	movq	%rax, -408(%rbp)
	movl	$.LC15, %eax
	movq	%rax, -384(%rbp)
	movq	$.LC2, -376(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -368(%rbp)
	movl	$1, -352(%rbp)
	movl	%ebx, -348(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, -360(%rbp)
	movq	-416(%rbp), %r8
	movq	-408(%rbp), %rdi
	movq	-384(%rbp), %rdx
	movq	-376(%rbp), %rcx
	movq	-368(%rbp), %rsi
	movq	-360(%rbp), %rax
	movq	%rdi, %r9
	movq	%rsi, %rdi
	movq	%rax, %rsi
	call	system__concat_2__str_concat_2
	movq	-136(%rbp), %rax
	movq	%rax, -336(%rbp)
	movl	$1, -320(%rbp)
	movl	%ebx, -316(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	-336(%rbp), %rdx
	movq	-328(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rax, %rsi
	call	ada__text_io__put_line__2
.LEHE1:
	movl	$1, %eax
.L19:
.LEHB2:
	movq	%r14, %rsp
	cmpl	$1, %eax
	jne	.L24
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	ada__real_time__Osubtract__2
	movq	%rax, %rdi
	call	ada__real_time__to_duration
	movq	%rax, -496(%rbp)
	fildq	-496(%rbp)
	fldt	.LC16(%rip)
	fmulp	%st, %st(1)
	fstps	-484(%rbp)
.LEHE2:
	movss	-484(%rbp), %xmm0
	movss	%xmm0, -92(%rbp)
	movq	%rsp, %rax
	movq	%rax, %r13
	leaq	-480(%rbp), %rax
	movq	%rax, -304(%rbp)
	movq	$.LC3, -296(%rbp)
.LEHB3:
	flds	-92(%rbp)
	fstpt	-512(%rbp)
	movq	-512(%rbp), %rax
	movl	-504(%rbp), %edx
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rcx
	movq	%rax, (%rsp)
	movl	%edx, 8(%rsp)
	movl	$6, %edx
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	call	system__img_real__image_floating_point
	movl	%eax, %edx
	movl	%edx, %eax
	movl	$0, %ecx
	testl	%eax, %eax
	cmovs	%ecx, %eax
	addl	$14, %eax
	leal	9(%rax), %ebx
	movl	%ebx, -140(%rbp)
	movl	$0, %eax
	testl	%ebx, %ebx
	cmovns	%ebx, %eax
	cltq
	movq	%rax, -152(%rbp)
	movl	$0, %eax
	testl	%ebx, %ebx
	cmovns	%ebx, %eax
	cltq
	addq	$15, %rax
	addq	$15, %rax
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	16(%rsp), %rax
	addq	$15, %rax
	shrq	$4, %rax
	salq	$4, %rax
	movq	%rax, -160(%rbp)
	movl	$.LC17, %eax
	movq	%rax, -288(%rbp)
	movq	$.LC4, -280(%rbp)
	leaq	-480(%rbp), %rax
	movq	%rax, -272(%rbp)
	movl	$1, -256(%rbp)
	movl	%edx, %eax
	movl	%eax, -252(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -264(%rbp)
	movl	$.LC18, %eax
	movq	%rax, -240(%rbp)
	movq	$.LC5, -232(%rbp)
	movq	-160(%rbp), %rax
	movq	%rax, -224(%rbp)
	movl	$1, -208(%rbp)
	movl	%ebx, -204(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-272(%rbp), %r8
	movq	-264(%rbp), %r9
	movq	-240(%rbp), %rdx
	movq	-232(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rax
	movq	-288(%rbp), %rdi
	movq	%rdi, (%rsp)
	movq	-280(%rbp), %rdi
	movq	%rdi, 8(%rsp)
	movq	%rsi, %rdi
	movq	%rax, %rsi
	call	system__concat_3__str_concat_3
	movq	-160(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$1, -176(%rbp)
	movl	%ebx, -172(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rax, %rsi
	call	ada__text_io__put_line__2
.LEHE3:
	movl	$1, %eax
.L21:
.LEHB4:
	movq	%r13, %rsp
	cmpl	$1, %eax
	je	.L5
	jmp	.L25
.L22:
	movq	%rax, %r13
	movl	$0, %eax
	jmp	.L19
.L24:
	movq	%r13, %rax
	movq	%rax, %rdi
	call	_Unwind_Resume
.L23:
	movq	%rax, %r12
	movl	$0, %eax
	jmp	.L21
.L25:
	movq	%r12, %rax
	movq	%rax, %rdi
	call	_Unwind_Resume
.L5:
	leaq	-32(%rbp), %rsp
	addq	$0, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	leave
	.cfi_def_cfa 7, 8
.LEHE4:
	ret
	.cfi_endproc
.LFE1:
	.size	_ada_mandelbenchada, .-_ada_mandelbenchada
.globl __gnat_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA1:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1-.LLSDACSB1
.LLSDACSB1:
	.uleb128 .LEHB0-.LFB1
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB1
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L22-.LFB1
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB1
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB3-.LFB1
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L23-.LFB1
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB1
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1:
	.text
	.section	.rodata
	.align 4
.LC6:
	.long	1215570944
	.align 4
.LC7:
	.long	1228348160
	.align 4
.LC9:
	.long	1101004800
	.align 16
.LC10:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.align 4
.LC11:
	.long	1109393408
	.align 4
.LC12:
	.long	1056964608
	.align 4
.LC13:
	.long	1082130432
	.align 16
.LC16:
	.long	917808535
	.long	2305843009
	.long	16353
	.long	0
	.ident	"GCC: (GNU) 4.5.3 20110419 for GNAT GPL 2011 (20110419)"
	.section	.note.GNU-stack,"",@progbits
